﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public AudioClip[] enviorment;
    public AudioClip[] enemies;
    public AudioClip[] player;
    public AudioClip[] demonVoiceMovingWalls;
    public AudioClip[] demonVoicePlayerDies;
    public AudioClip[] demonVoiceStartLevel;

    public AudioSource[] sourceAud;

    void Awake()
    {
        DemonStartLevel();
        instance = this;
    }
    /// <summary>
    /// MONSTERS SPAWN SOUNDS
    /// </summary>
    public void FlyingMonsterSpawn()
    {
        sourceAud[0].clip = enemies[0];
        sourceAud[0].Play();
    }
    public void SimpleMonsterSpawn()
    {
        sourceAud[0].clip = enemies[1];
        sourceAud[0].Play();
    }
    public void SkeletMonsterSpawn()
    {
        sourceAud[0].clip = enemies[2];
        sourceAud[0].Play();
    }
    /// <summary>
    /// ENVIORNMENT SOUNDS
    /// </summary>
    public void Wall_Moving()
    {
        sourceAud[1].clip = enviorment[0];
        sourceAud[1].Play();
    }
    public void LaserShot()
    {
        sourceAud[3].clip = enviorment[1];
        sourceAud[3].Play();
    }
    /// <summary>
    /// PLAYER SOUNDS
    /// </summary>
    public void PlayerAttacksEmpty()
    {
        sourceAud[2].clip = player[0];
        sourceAud[2].Play();
    }
    public void PlayerAttacksEnemy()
    {
        sourceAud[2].clip = player[1];
        sourceAud[2].Play();
    }
    public void PlayerAttcksSkelet()
    {
        sourceAud[2].clip = player[2];
        sourceAud[2].Play();
    }
    public void PlayerAttcksBorders()
    {
        sourceAud[2].clip = player[3];
        sourceAud[2].Play();
    }
    public void PlayerDies()
    {
        sourceAud[2].clip = player[4];
        sourceAud[2].Play();
    }
   /// <summary>
   /// DEMON SOUNDS
   /// </summary>
    public void DemonWallsMoving()
    {
        sourceAud[4].clip = demonVoiceMovingWalls[Random.Range(0, demonVoiceMovingWalls.Length)];
        sourceAud[4].Play();
    }
    public void DemonPlayerDies()
    {
        sourceAud[4].clip = demonVoicePlayerDies[Random.Range(0, demonVoicePlayerDies.Length)];
        sourceAud[4].Play();
    }
    public void DemonStartLevel()
    {
        sourceAud[4].clip = demonVoiceStartLevel[Random.Range(0, demonVoiceStartLevel.Length)];
        sourceAud[4].Play();
    }
}