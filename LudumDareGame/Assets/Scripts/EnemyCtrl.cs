﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCtrl : MonoBehaviour, IDamage
{
    public bool goForPlayer;
    private Transform player;

    public float speed = 5;
    public Vector2 dir;

    public int currDir = -1;
    public LayerMask mask;

    private AudioManager audioControl;

    private Animator anim;

    private List<int> directionIndex;

    private float timeTillNewDir = 0.5f;
    private float currTimeNewDir = 0.5f;

    public int lives = 1;
    private bool dontMove = false;

    private float moveAgain = 0f;


    private void Awake()
    {
        player = FindObjectOfType<CharacterCtrl>().transform;
        anim = GetComponent<Animator>();

        audioControl = AudioManager.instance;
    }

    private void Update()
    {
        if (!dontMove)
        {

            directionIndex = new List<int>();

            for (int i = 0; i < 4; i++)
            {
                directionIndex.Add(i);
            }

            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up, 0.5f, mask);

            if (hit)
                if (hit.collider.tag == "Wall")
                {
                    directionIndex.Remove(0);
                }

            RaycastHit2D hit1 = Physics2D.Raycast(transform.position, Vector2.down, 0.5f, mask);

            if (hit1)
                if (hit1.collider.tag == "Wall")
                {
                    directionIndex.Remove(1);
                }

            RaycastHit2D hit2 = Physics2D.Raycast(transform.position, Vector2.left, 0.5f, mask);

            if (hit2)
                if (hit2.collider.tag == "Wall")
                {
                    directionIndex.Remove(2);
                }

            RaycastHit2D hit3 = Physics2D.Raycast(transform.position, Vector2.right, 0.5f, mask);

            if (hit3)
                if (hit3.collider.tag == "Wall")
                {
                    directionIndex.Remove(3);
                }

            if (currTimeNewDir >= timeTillNewDir && directionIndex.Count > 0)
            {
                currTimeNewDir = 0;

                int index = Random.Range(0, directionIndex.Count);
                currDir = directionIndex[index];

                switch (currDir)
                {
                    case 0:
                        dir = Vector2.up;
                        break;

                    case 1:
                        dir = Vector2.down;
                        break;

                    case 2:
                        dir = Vector2.left;
                        break;

                    case 3:
                        dir = Vector2.right;
                        break;
                }
            }
            else
            {
                currTimeNewDir += Time.deltaTime;
            }
            Move();
        }
        else
        {

            if (moveAgain >= 1)
            {
                moveAgain = 0;
                dontMove = false;
            }
            else
            {
                moveAgain += Time.deltaTime;
            }
        }
    }

    public void Move()
    {
        GetComponent<Rigidbody2D>().velocity = dir * speed;
    }

    private Vector2 directionHit;

    public void SetDirection(Vector2 dir)
    {
        directionHit = dir;
    }

    public void GetDamage()
    {
        if (gameObject.name == "Enemy_1(Clone)" || gameObject.name == "Enemy_3(Clone)")
        {
            audioControl.PlayerAttacksEnemy();
        }
        else if(gameObject.name == "Enemy_2(Clone)")
        {
            audioControl.PlayerAttcksSkelet();
        }

        lives--;

        if (lives <= 0)
        {
            GameManager.instance.SpawnExplotion(transform.position);
            Debug.Log("enemy dead");
            Destroy(gameObject);
        }
        else
        {
            dontMove = true;
            GetComponent<Rigidbody2D>().velocity = directionHit * (speed * 10);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            anim.SetBool("Fight", true);
            collision.collider.GetComponent<IDamage>().GetDamage();
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            anim.SetBool("Fight", false);
        }
    }

    public void Destroy()
    {
        GameManager.instance.SpawnExplotion(transform.position);
        Debug.Log("enemy dead");
        Destroy(gameObject);
    }
}