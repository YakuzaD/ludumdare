﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour, IDamage
{
    public void GetDamage()
    {
        FindObjectOfType<Borders>().GoingToInitialPosition();
    }

    public void Destroy()
    { }
}
