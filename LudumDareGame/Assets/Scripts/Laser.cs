﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public GameObject[] laser;
    public bool showLaser = false;
    public bool eyeReady = false;

    public float tillNextLaser = 0;
    private float currLaser = 0;

    public float durrationLaser = 2;
    private float currDurrationLaser = 0;

    public float durrationEye = 1;
    private float currDurrationEye = 0;

    public Transform laser1;
    public Transform laser2;

    public LayerMask layer;
    public SpriteRenderer eyes;

    public bool playOnce = false;

    private AudioManager audControl;

	private void Start()
	{
        audControl = AudioManager.instance;
	}

	private void Update()
    {
        if (currLaser >= tillNextLaser)
        {
            showLaser = true;
        }
        else
        {
            currLaser += Time.deltaTime;
        }

        if (showLaser)
        {
            if(!playOnce){
                playOnce = true;
                audControl.LaserShot();
            }

            if (currDurrationEye >= durrationEye)
            {
                eyeReady = true;
            }
            else
            {
                currDurrationEye += Time.deltaTime;
            }

            eyes.color = new Color(1, 1, 1, currDurrationEye);
        }

        if (showLaser && eyeReady)
        {
            laser[0].SetActive(true);
            laser[1].SetActive(true);

            RaycastHit2D hit1 = Physics2D.Raycast(laser1.position, Vector2.down, 50f, layer);

            if (hit1)
            {
                hit1.collider.GetComponent<IDamage>().Destroy();
            }

            RaycastHit2D hit2 = Physics2D.Raycast(laser2.position, Vector2.down, 50f, layer);

            if (hit2)
            {
                hit2.collider.GetComponent<IDamage>().GetDamage();
            }

            if (currDurrationLaser >= durrationLaser)
            {
                currDurrationLaser = 0;
                currLaser = 0;
                currDurrationEye = 0;
                showLaser = false;
                eyeReady = false;
                playOnce = false;
                eyes.color = new Color(1, 1, 1, 0);
            }
            else
            {
                currDurrationLaser += Time.deltaTime;
            }

        }
        else
        {
            laser[0].SetActive(false);
            laser[1].SetActive(false);
        }

    }

}
