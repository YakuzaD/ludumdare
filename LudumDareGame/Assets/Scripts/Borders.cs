﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Borders : MonoBehaviour
{
    public static string hitWall;

    [SerializeField]
    GameObject leftBorder;
    [SerializeField]
    GameObject rightBorder;
    [SerializeField]
    float speed = 1.0f;
    [SerializeField]
    float delay = 3.0f;
    [SerializeField]
    float timeToWait = 1.0f;

    private Vector2 initialPosLeftBorder;
    private Vector2 initialPosRightBorder;
    private bool playerHitTheWall = false;
    private float currentTime = 0;
    private float currentTimeLeftRight = 0;

    private bool stopMove = false;
    private AudioManager audioControl;

    void Start()
    {
        audioControl = AudioManager.instance;

        initialPosLeftBorder = leftBorder.transform.position;
        initialPosRightBorder = rightBorder.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!stopMove)
        {
            currentTime += Time.deltaTime;
            if (currentTime >= Time.deltaTime + delay && currentTime < Time.deltaTime + delay + timeToWait)
            {
                GoingToCenter();
                playerHitTheWall = false;
            }
            else if (currentTime > Time.deltaTime + delay + timeToWait)
            {
                currentTime = 0;
            }
            PlayerHitChecker();
        }
    }

    void PlayerHitChecker()
    {

        if (playerHitTheWall == true)
        {
            if (hitWall == "Left")
            {
                GoingToInitialPositionLeft();
            }
            else if (hitWall == "Right")
            {
                GoingToInitialPositionRight();
            }
        }
    }

    void GoingToCenter()
    {
        audioControl.DemonWallsMoving();
        audioControl.Wall_Moving();

        Vector2 left = leftBorder.transform.position;
        left += Vector2.right * speed * Time.deltaTime;
        Vector2 right = rightBorder.transform.position;
        right += Vector2.left * speed * Time.deltaTime;

        if (Mathf.Abs(Vector2.Distance(left, right)) > 2)
        {
            leftBorder.transform.Translate(Vector2.right * speed * Time.deltaTime);
            rightBorder.transform.Translate(Vector2.left * speed * Time.deltaTime);
        }
        else
        {
            stopMove = true;
            FindObjectOfType<GameManager>().CheckBorderToPlayer();
        }
    }

    //public void GoingToInitialPositionLeft()
    //{
    //    if (leftBorder.transform.position.x > initialPosLeftBorder.x)
    //    {
    //        print("Left" + currentTime);
    //        currentTimeLeftRight+=Time.deltaTime; 
    //        if (currentTimeLeftRight > timeToWait + Time.deltaTime)
    //        {
    //            playerHitTheWall = false;
    //            currentTimeLeftRight = 0;
    //        }
    //        else if(currentTimeLeftRight >= 0 && currentTimeLeftRight < timeToWait + Time.deltaTime)
    //        {
    //            leftBorder.transform.Translate(Vector2.left * speed * Time.deltaTime);
    //        }
    //        //currentTime = 0;
    //        //playerHitTheWall = false;
    //    }

    //}


    public void GoingToInitialPositionRight()
    {
        if (rightBorder.transform.position.x <= initialPosRightBorder.x)
        {
            rightBorder.transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
    }

    public void GoingToInitialPositionLeft()
    {
        if (leftBorder.transform.position.x >= initialPosLeftBorder.x)
        {
            leftBorder.transform.Translate(Vector2.left * speed * Time.deltaTime);
        }
    }


    //public void GoingToInitialPositionRight()
    //{
    //    print("Right" + currentTime);
    //    currentTimeLeftRight+=Time.deltaTime; 
    //    if (rightBorder.transform.position.x < initialPosRightBorder.x)
    //    {
    //        if (currentTimeLeftRight > timeToWait + Time.deltaTime)
    //        {
    //            playerHitTheWall = false;
    //            currentTimeLeftRight = 0;
    //        }
    //        else if (currentTimeLeftRight >= 0 && currentTimeLeftRight < timeToWait + Time.deltaTime)
    //        {
    //            rightBorder.transform.Translate(Vector2.right * speed * Time.deltaTime);
    //        }
    //        //currentTime = 0;
    //        //playerHitTheWall = false;
    //    }
    //}

    public void GoingToInitialPositionBorders(){
        currentTimeLeftRight += Time.deltaTime;
        if (leftBorder.transform.position.x >= initialPosLeftBorder.x)
        {
            leftBorder.transform.Translate(Vector2.left * speed * Time.deltaTime);
        }
        if(rightBorder.transform.position.x <= initialPosRightBorder.x){
            rightBorder.transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
    }

    public void GoingToInitialPosition()
    {
        playerHitTheWall = true;
    }

}