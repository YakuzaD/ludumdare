﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCtrl : MonoBehaviour, IDamage
{
    public Rigidbody2D player_rb;
    private float speed = 4f;

    private float timeDelay = 0.25f;
    private float currentTime = 0.25f;
    private Animator characterAnim;

    public Transform startRayPos;

    public LayerMask layer;
    private int sign = 1;
    private AudioManager audioControl;

    void Start()
    {
        characterAnim = GetComponent<Animator>();

        audioControl = AudioManager.instance;
    }

    void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        if (moveHorizontal == 0.0f && moveVertical == 0.0f)
        {
            characterAnim.SetBool("Moving", false);
        }
        else
        {
            characterAnim.SetBool("Moving", true);
        }

        if (moveHorizontal < 0)
        {
            sign = -1;
            transform.localScale = new Vector3(-1f, 1f, 1);
        }
        else if (moveHorizontal > 0)
        {
            sign = 1;
            transform.localScale = new Vector3(1f, 1f, 1);
        }

        // player_rb.velocity = new Vector2(moveHorizontal * speed, moveVertical * speed)

        player_rb.transform.Translate(new Vector3(moveHorizontal * speed * Time.deltaTime, moveVertical * speed * Time.deltaTime, 0));

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (currentTime > timeDelay)
            {
                characterAnim.SetBool("Moving", false);
                characterAnim.SetBool("Fight", true);
                Attack();
            }
        }

        currentTime += Time.deltaTime;

        Debug.DrawRay(startRayPos.position, Vector2.right * sign);
        Debug.DrawRay(startRayPos.position, new Vector2(1, 0.7f) * sign);
        Debug.DrawRay(startRayPos.position, new Vector2(1, -0.7f) * sign);
    }

    private void FinishAttack()
    {
        characterAnim.SetBool("Fight", false);
    }

    private void Attack()
    {
        RaycastHit2D hit = Physics2D.Raycast(startRayPos.position, Vector2.right * sign, 1.5f, layer);

        if (hit)
        {
            MakeHit(hit);
            return;
        }

        RaycastHit2D hit1 = Physics2D.Raycast(startRayPos.position, new Vector2(1, 0.7f) * sign, 1.5f, layer);

        if (hit1)
        {
            MakeHit(hit1);
            return;
        }

        RaycastHit2D hit2 = Physics2D.Raycast(startRayPos.position, new Vector2(1, -0.7f) * sign, 1.5f, layer);

        if (hit2)
        {
            MakeHit(hit2);
            return;
        }

        if (hit == false && hit1 == false && hit2 == false)
        {
            audioControl.PlayerAttacksEmpty();
        }
    }

    private void MakeHit(RaycastHit2D hit)
    {
        if (hit.collider.tag == "Enemy")
        {
            if (hit.collider.name == "Left")
            {
                Borders.hitWall = "Left";
                audioControl.PlayerAttcksBorders();
            }
            else if (hit.collider.name == "Right")
            {
                Borders.hitWall = "Right";
                audioControl.PlayerAttcksBorders();
            }
            else if (hit.collider.name == "Enemy")
            {
                audioControl.PlayerAttacksEnemy();
            }

            if (hit.collider.GetComponent<EnemyCtrl>())
            {
                hit.collider.GetComponent<EnemyCtrl>().SetDirection(sign * Vector2.right);
            }

            hit.collider.GetComponent<IDamage>().GetDamage();
        }
    }

    public void GetDamage()
    {
        audioControl.DemonPlayerDies();
        audioControl.PlayerDies();
        Debug.Log("player dead");
        FindObjectOfType<GameUI>().ShowScorePopUp();
        GameManager.instance.SpawnExplotion(transform.position);
        Destroy(gameObject);
    }

    public void Destroy() { }
}
