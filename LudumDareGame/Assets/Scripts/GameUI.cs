﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour
{
    public Image buttonSound;
    public Text score;

    public Sprite onSound;
    public Sprite offSound;

    public GameObject fade;
    public GameObject popUp;
    public GameObject scorePopUp;

    public float gameTime;
    private bool stopTime = false;

    private void Update()
    {
        if (!stopTime)
            gameTime += Time.deltaTime;
    }
    public void ShowScorePopUp()
    {
        fade.SetActive(true);
        popUp.SetActive(true);
        scorePopUp.SetActive(true);

        stopTime = true;
        score.text = "Your time: " + (int)gameTime + " seconds";
    }

    public void MuteSound()
    {
        if (AudioListener.volume == 1)
        {
            AudioListener.volume = 0;
            buttonSound.sprite = offSound;
        }
        else
        {
            AudioListener.volume = 1;
            buttonSound.sprite = onSound;
        }

        GameObject myEventSystem = GameObject.Find("EventSystem");
        myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void Quit()
    {
        SceneManager.LoadScene("Menu");
    }

}
