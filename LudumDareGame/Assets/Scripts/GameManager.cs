﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject player_pref;
    public GameObject[] enemy_pref;
    public GameObject explotion_pref;

    public Transform leftBorder;
    public Transform rightBorder;
    public Transform enemyHold;
    public Transform[] playerRadius;

    public AudioManager audioControl;

    private GameObject player;
    private bool stopSpawn = false;
    private float spawnDelay = 3f;
    private float currSpawnTime = 2f;
    private float spawnDelayInitialValue;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        //player = Instantiate(player_pref);
        player = FindObjectOfType<CharacterCtrl>().gameObject;
        spawnDelayInitialValue = spawnDelay;
    }

    private void Update()
    {
        if (currSpawnTime >= spawnDelay && !stopSpawn)
        {
            if (player != null)
            {
                SpawnEnemy();
            }
            currSpawnTime = 0;
        }
        else
        {
            currSpawnTime += Time.deltaTime;
        }

    }

    public void CheckBorderToPlayer()
    {
        stopSpawn = true;

        if (player != null)
        {
            player.GetComponent<IDamage>().GetDamage();
        }

        EnemyCtrl[] enemys = FindObjectsOfType<EnemyCtrl>();

        if (enemys.Length > 0)
        {
            for (int i = 0; i < enemys.Length; i++)
            {
                enemys[i].Destroy();
            }
        }

    }

    private void SpawnEnemy()
    {

        Vector2 randomPositionForEnemy = new Vector2(Random.Range(leftBorder.transform.position.x + 2f, rightBorder.transform.position.x - 2f),
                                                     Random.Range(-4f, 2f));
        if ((randomPositionForEnemy.y >= playerRadius[0].position.x || randomPositionForEnemy.y <= playerRadius[1].position.y) &&
            (randomPositionForEnemy.x <= playerRadius[2].position.x || randomPositionForEnemy.x >= playerRadius[3].position.x)
           )
        {
            int rand = Random.Range(0, 100);

            if (rand <= 50)
            {
                audioControl.FlyingMonsterSpawn();
                rand = 0;
            }
            else if (rand <= 80)
            {
                audioControl.SimpleMonsterSpawn();
                rand = 1;
            }
            else if (rand <= 100)
            {
                audioControl.SkeletMonsterSpawn();
                rand = 2;
            }

            if (rand >= enemy_pref.Length)
            {
                rand = 0;
            }

            GameObject gm = Instantiate(enemy_pref[rand], randomPositionForEnemy, Quaternion.identity);
            gm.transform.SetParent(enemyHold);

            spawnDelay = spawnDelayInitialValue;
        }
        else
        {
            spawnDelay = 0;
        }
    }

    public void SpawnExplotion(Vector3 pos)
    {
        GameObject gm = Instantiate(explotion_pref);
        gm.transform.position = pos;
    }

}